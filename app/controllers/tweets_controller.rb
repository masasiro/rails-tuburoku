class TweetsController < ApplicationController
  def index
    @tweet = Tweet.new
    @tweets = Tweet.all
  end
  
  def show
    @tweet = Tweet.find(params[:id])
  end
  
  def create
    @tweet = Tweet.new(params[:tweet])
    if 0 < @tweet.content.length
      if @tweet.save
        redirect_to tweets_path
      else
        redirect_to tweets_path, notice: 'Tweet Failed.'
      end
    else
      @tweet = nil
    end
  end
  
  def edit
    @tweet = Tweet.find(params[:id])
  end
  
  def update
    @tweet = Tweet.find(params[:id])
    if @tweet.update_attributes(params[:tweet])
      redirect_to tweets_path, notice: "Tweet Updated."
    else
      render action: 'edit'
    end
  end
  
  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    redirect_to tweets_path
  end
end

